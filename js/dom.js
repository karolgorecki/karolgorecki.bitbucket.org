jewel.dom = (function(){
    var $ = Sizzle;

    function hasClass(el, cName){
        var regex = new RegExp("(^|\\s)" + cName + "(\\s|$)");
        return regex.test(el.className);
    }

    function addClass(el, cName) {
        if(!hasClass(el, cName)) {
            el.className += " " + cName;
        }
    }

    function removeClass(el, cName){
        var regex = new RegExp("(^|\\s)" + cName + "(\\s|$)");
        el.className = el.className.replace(regex," ")
    }

    function bind(el,evt,handler){
        if(typeof(el) == "string"){
            el = $(el)[0];
        }

        el.addEventListener(evt,handler,false);
    }

    return {
        $: $,
        hasClass:       hasClass,
        addClass:       addClass,
        removeClass:    removeClass,
        bind:           bind
    }
})();