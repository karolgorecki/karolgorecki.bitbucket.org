var jewel = {
        screens: {}
};

window.addEventListener("load", function(){
    Modernizr.addTest("standalone", function(){
        return (window.navigator.standalone != false);
    });
    Modernizr.load([{
        load: [
        "js/sizzle.js",
        "js/dom.js",
        "js/game.js",

        ],

    },
    {
        test:     Modernizr.standalone,
        yep:      "js/intro.screen.js",
        nope:     "js/install.screen.js",
        complete: function(){
            jewel.game.setup();
            if(Modernizr.standalone) {
                jewel.game.showScreen("intro");
            } else {
                jewel.game.showScreen("install");
            }
        }
    }

    ]);

    if(Modernizr.standalone) {
        Modernizr.load([
            {
                load: [
                    "js/menu.screen.js"
                ]
            }
        ]);
    }
},false);