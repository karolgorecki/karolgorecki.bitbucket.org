jewel.screens["intro"] = (function(){
    var game     = jewel.game,
        dom      = jewel.dom,
        firstRun = true;

    function setup(){
        dom.bind("#start-game", "click", function(){
            game.showScreen("menu");
        });
    }

    function run() {
        if(firstRun) {
            setup();
            firstRun = false;
        }
    }

    return {
        run: run
    }

})();