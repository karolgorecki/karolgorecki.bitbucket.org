jewel.game = (function(){
    var dom = jewel.dom,
        $   = dom.$;

        function setup(){
            dom.bind(document,"touchmove",function(e){
                e.preventDefault();
                if(/Android/.test(navigator.userAgent)) {
                    $("html")[0].style.height = "200%";
                    setTimeout(function(){
                        window.scrollTo(0, 1);
                    }, 0);
                }
            });
        }

        function showScreen(screenId){
            var screenId     = screenId || "intro", //show intro by default
                activeScreen = $("#game-container .screen.active")[0],
                screen       = $("#" + screenId)[0];

                if(activeScreen)
                    dom.removeClass(activeScreen,"active");

                jewel.screens[screenId].run();
                dom.addClass(screen,"active");
        }   

        return {
            showScreen: showScreen,
            setup:      setup
        }
})();